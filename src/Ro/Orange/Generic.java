package Ro.Orange;


//A public class called Generic will contain one parameter Employee
public class Generic<Employee> {

        //Declare an object of type Employee
        private Employee details;


        //This is the correct constructor
        Generic(Employee attribute){
                this.details = attribute;
        }

        //This is not a constructor, this is a function. THe code below is from the original version
        /*//An constructor for Employee
        public void Employee(Employee details){
            this.details = details;

        }*/

        //And a get method to return the object
        public Employee getDetails() {
        return details;
        }
}
