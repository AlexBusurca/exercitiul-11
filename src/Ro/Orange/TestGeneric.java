package Ro.Orange;

public class TestGeneric {

    public static void main(String[] args) {
    //Two instance of Strings type from Generic Class
	Generic<String> e1 = new Generic<>("Samuel Jackson");
	Generic<String> e2 = new Generic<>("cashier");

	//Old way, where i had a function instead of a constructor
	//e1.Employee("Samuel Jackson");
	//e2.Employee("cashier");

    //Build two sentences and print them
        System.out.println("My name is " + e1.getDetails() +".");
        System.out.println("I work as " + e2.getDetails() + " at Plaza Hotel.");

    //One instance of Integer type from Generic Class
	Generic<Integer> e3 = new Generic<>(2000);
	//Old way, where i had a function instead of a constructor
	//e3.Employee(2000);

	//Build one sentence and print it
        System.out.println("My salary is valued somewhere around " + e3.getDetails() + " dollars.");


    }
}
